# hevc_H.265_video_converter

Compress all "*.mp4" videos in a directory to 1/4 of original size by encoding them with modern H.265 (HEVC) format. This repo contains two different scripts: 

- **hevcify.py**: Convert videos to H.265

    - Adds "*h.mp4" to the end of original video name to indicate that it is converted to HEVC.
        
- **goprocess.py**: Convert GoPro videos to H.265
    
    - Merges chaptered and looping videos to one long video. Renames new video with creation time.
    - Only works cameras GoPro Hero <= 5 due to the file naming conventions
    
Note: Original file are removed by default (unless ```remove_original_files``` is set to ```False``` in source code).


## Dependencies
Install MLT dependencies. On Debian run:
```sudo apt-get install melt ladspa-sdk swh-plugins``` .

## Usage
Navigate to directory that contains videos. Run

```hevcify.py```

or

```goprocess.py```

and wait quite long (processing a minute long may take 10 minutes). All videos that are not yet converted, will be coverted to HEVC. File name indicates which videos are converted converted.

## Concerns
Even though HEVC is ISO-standard, its patent has royalty fees. Implementors of open source HEVC encoders need to pay for distributing their software. Therefore **HEVC is strongly unrecommended format**. I did not know this at the moment I did this script. WebM AV1 is has comparable performance to HEVC, but is totally royalty free, so use that instead. (TODO myself: convert this script to use AV1.)
