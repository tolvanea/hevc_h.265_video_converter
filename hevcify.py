#!/usr/bin/python3 

"""
This script: 
    - Converts all videos in a folder to h.265-format (HEVC).
    - Rename e.g. VID_20190418_062155.mp4 to VID_20190418_062155h.mp4 (add "h")
    - Removes old videos.
    
Alpi Tolvanen 2019
Licence: MIT or WTFPL
"""

remove_original_files = True  # Change this to 'False' to keep original videos

import os
import apt
from datetime import datetime
import subprocess
import shlex
import time

def check_dependencies():
    cache = apt.Cache()
    for package in ["melt", "ladspa-sdk", "swh-plugins"]:
        if not cache[package].is_installed:
            print("Not all requirements installed. Please install them with:")
            print("sudo apt-get install melt ladspa-sdk swh-plugins")
            exit(1)
        # Enzyme (the video metadata extractor) does not work somewhy.
        # I have to add "h" to file that indicates that it is already processed
#    try:
#        import enzyme
#    except ImportError:
#        print("Not all requirements installed. Please install them with:")
#        print("pip3 install enzyme")
#        exit(1)

def group_videos(no_merging=False):
    vids = []
    for f in sorted(os.listdir(".")):
        if not( os.path.isfile(f) 
                and len(f) > 5
                and ((f[-4:]==".MP4") or (f[-4:]==".mp4"))
                and (f[-5]!="h")):  # Already proccessed to HEVC
            continue
        
        # Enzyme (the video metadata extractor) does not work somewhy.
        # I have to add "h" to file that indicates that it is already processed
#        import enzyme
#        with open(f, 'rb') as fil:
#            mkv = enzyme.MKV(fil)
#        print(mkv.video_tracks)
#        exit(1)
        
        vids.append(f)
            
    return vids

        
def encode_videos(vids):
    for vid in vids:
        out_name = vid[:-4] + "h" + vid[-4:]
        
        command = " ".join(["melt", vid, "-progress -consumer avformat:"+out_name, "acodec=aac vcodec=libx265"])
        print(command)
        subprocess.run(command, shell=True, check=True)
        
        if remove_original_files:
            os.remove(vid)
            print("\n\nProcessed (and deleted)" + str(vid)) 
        else:
            print("\n\nProcessed " + str(vid)) 
            
        print("to " + out_name + ".\n\n")


def main():
    check_dependencies()
    vids = group_videos()
    encode_videos(vids)
    
if __name__ == "__main__":
    main()
        
