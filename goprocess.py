#!/usr/bin/python3 

"""
This script: 
    - Reads all gopro videos in current directory, and converts them all to 
      h.265-format (HEVC). 
    - Concatenates chaptered and looping videos to one video. 
    - Rename new video with creation time. Modification time is used if videos 
      are not exported with gopro-app (=file name does not contain epoch time).
      "GOPR1234_12341231123.mp4" --> "VID_2019-04-26T17.40.45.mp4"
    - Removes old videos.

Due to the gopro file naming conventions, this works with models earlier than HERO6:
    HD HERO2, HERO3, HERO3+, HERO (2014), HERO Session, HERO4, HERO5 Black, 
    HERO5 Session, HERO (2018)
    
Alpi Tolvanen 2019
Licence: MIT or WTFPL
"""

remove_original_files = True  # Change this to 'False' to keep original videos

import os
import apt
from datetime import datetime
import subprocess
import shlex
import time

#  #python-mlt3

def check_dependencies():
    cache = apt.Cache()
    for package in ["melt", "ladspa-sdk", "swh-plugins"]:
        if not cache[package].is_installed:
            print("Not all requirements installed. Please install them with:")
            print("sudo apt-get install melt ladspa-sdk swh-plugins")
            exit(1)

def group_videos(no_merging=False):
    vids = {}
    for f in sorted(os.listdir(".")):
        if not( os.path.isfile(f) 
            and len(f) > 4
            and f[0]=="G" 
            and (f[-4:]==".MP4") or (f[-4:]==".mp4")):
            continue
        
        if f[:4] == "GOPR" or no_merging:
            tag = f[4:8]
        elif f[:2] == "GP":
            tag = f[2:4]
        elif f[:1] == "G":
            tag = f[1:4]
        else:
            continue # Something wrong with file naming --> not gopro-video
            
        if tag in vids:
            vids[tag].append(f)
        else:
            vids[tag] = [f]
            
    return vids


# # def run_command(command):
# #     # https://www.endpoint.com/blog/2015/01/28/getting-realtime-output-using-python
# #     process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, text=True)
# #     while True:
# #         output = process.stdout.readline()
# #         if output == '' and process.poll() is not None:
# #             break
# #         if output:
# #             #print(output.strip())
# #             #print(str(output.strip()).replace("\033", "/033"))
# #             print("??")
# #         if "1 frames left in the queue on closing" in str(output):
# #             break
# #     rc = process.poll()
# #     if rc is None:
# #         assert(False) ## Process did not end jet
# #     return rc
# 
# 
# def run_command(command):
#     # https://www.endpoint.com/blog/2015/01/28/getting-realtime-output-using-python
#     process = subprocess.Popen(command, stdin=subprocess.PIPE, 
#                                #stdout=subprocess.PIPE, 
#                                shell=True, 
#                                universal_newlines=True)
#     while True:
#         if process.poll() is not None:
#             print("Exited.")
#             break
#         time.sleep()
#         proc.stdin.write("\n")
# #        try:
# #            process.communicate(, timeout=2)
# #        except subprocess.TimeoutExpired:
# #            continue
#     return None
        
def encode_videos(vids):
    processed_times = []  # Avoid renaming with same name, (vids with same time)
    for tag in vids:
        first_vid = vids[tag][0]
        if len(first_vid) > 22: # File is exported with app, and name contains unix time
            epoch_time = int(first_vid[9:22]) / 1000
            time_str = datetime.fromtimestamp(epoch_time).isoformat()[:19]
        else:
            pass # TODO
            epoch_time = os.stat(first_vid).st_ctime
            time_str = datetime.fromtimestamp(epoch_time).isoformat()[:19]
            
        if time_str in processed_times:
            count = processed_times.count(time_str)
            out_name = "VID_" + time_str + "-" + str(count) + ".mp4"
        else:
            out_name = "VID_" + time_str + ".mp4"
            
        out_name = out_name.replace(":", ".")  # Melt doesn't work with names containing ':'
        
        videos = " ".join(vids[tag])
        command = " ".join(["melt", videos, "-progress -consumer avformat:"+out_name, "acodec=aac vcodec=libx265"])
        print(command)
        #run_command(command)
        subprocess.run(command, shell=True, check=True)
        
        if remove_original_files:
            for vid in vids[tag]:
                os.remove(vid)
            print("\n\nProcessed (and deleted)" + str(vids[tag])) 
        else:
            print("\n\nProcessed " + str(vids[tag])) 
            
        print("to " + out_name + ".\n\n")


def main():
    check_dependencies()
    vids = group_videos()
    encode_videos(vids)
    
if __name__ == "__main__":
    main()
        
